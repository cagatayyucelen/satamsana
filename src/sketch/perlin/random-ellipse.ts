import p5 from "p5";
import { title } from "../../util/text";

export const randomEllipse = (p: p5) => {
  p.setup = () => {
    const canvas = p.createCanvas(400, 190);
    canvas.parent("perlin-random-ellipse");
  };

  p.draw = () => {
    p.background(20);
    title(p, "Random");
    var xRandom = p.random(p.width);

    p.ellipse(xRandom, 100, 24, 24);
  };
};

import p5 from "p5";
import { title } from "../../util/text";

export const perlinGraph2D = (p: p5) => {
  const titleSpacing = 35;
  const inc = 0.01;
  let position = 0;

  p.setup = () => {
    const canvas = p.createCanvas(820, 400);
    canvas.parent("perlin-graph-2d");
    p.pixelDensity(1);
  };

  p.draw = () => {
    p.background(20);
    title(p, "Perlin Noise 2D");

    let yoff = position;

    p.loadPixels();
    for (let y = titleSpacing; y < p.height; y++) {
      let xoff = position;
      for (let x = 0; x < p.width; x++) {
        const index = (x + y * p.width) * 4;
        const r = p.noise(xoff, yoff) * 255;
        p.pixels[index + 0] = r;
        p.pixels[index + 1] = r;
        p.pixels[index + 2] = r;
        p.pixels[index + 3] = 255;

        xoff += inc;
      }
      yoff += inc;
    }

    position += inc;

    p.updatePixels();
    p.noLoop();
  };
};

import p5 from "p5";
import { title, footer } from "../../util/text";

export const perlinNoiseEllipse = (p: p5) => {
  let xoff = 0;
  const inc = 0.01;

  p.setup = () => {
    const canvas = p.createCanvas(400, 190);
    canvas.parent("perlin-random-ellipse");
  };

  p.draw = () => {
    p.background(20);
    title(p, "Perlin Noise");

    const formattedXoff = p.nf(xoff, 0, 2);
    footer(p, "[ xoff= " + formattedXoff + " ]");

    var xPerlin = p.map(p.noise(xoff), 0, 1, 0, p.width);

    p.ellipse(xPerlin, 100, 24, 24);

    xoff += inc;
  };
};

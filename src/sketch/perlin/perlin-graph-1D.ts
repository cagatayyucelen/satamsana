import p5 from "p5";
import { title, footer } from "../../util/text";
import { mapToScreenHeight } from "../../util/screen";

export const perlinGraph1D = (p: p5) => {
  const inc = 0.01;
  let position = 0;

  p.setup = () => {
    const canvas = p.createCanvas(400, 400);
    canvas.parent("perlin-graph");
  };

  p.draw = () => {
    p.background(20);

    let xoff = position;

    const formattedXoff = p.nf(xoff, 0, 2);

    title(p, "Perlin Noise Graph");
    footer(p, "[ xoff= " + formattedXoff + " | inc= " + inc + " ]");

    p.beginShape();

    for (let x = 0; x < p.width; x++) {
      const y = mapToScreenHeight(p, p.noise(xoff));
      p.vertex(x, y);
      drawIndicator(x, y);
      xoff += inc;
    }

    position += inc;
    resetStyle();
    p.endShape();
  };

  const drawIndicator = (x: number, y: number) => {
    if (x === p.width - 1) {
      p.stroke(63, 9, 215);
      p.strokeWeight(3);
      p.fill(47, 79, 79);
      p.ellipse(x, y, 10, 10);
    }
  };

  const resetStyle = () => {
    p.noFill();
    p.stroke(255);
    p.strokeWeight(1);
  };
};

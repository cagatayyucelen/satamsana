import p5 from "p5";
import { title } from "../../util/text";

export const perlinNoiseEllipse = (p: p5) => {
  let xoff = 0;

  p.setup = () => {
    const canvas = p.createCanvas(400, 200);
    canvas.parent("perlin-random-ellipse");
  };

  p.draw = () => {
    p.background(20);
    title(p, "Perlin Noise");

    var xPerlin = p.map(p.noise(xoff), 0, 1, 0, p.width);

    p.ellipse(xPerlin, 100, 24, 24);

    xoff += 0.01;
  };
};

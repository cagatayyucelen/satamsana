import p5 from "p5";

import { perlinNoiseEllipse } from "./perlin-ellipse";
import { randomEllipse } from "./random-ellipse";
import { perlinGraph1D } from "./perlin-graph-1D";
import { perlinGraph2D } from "./perlin-graph-2D";

const perlinNoiseEllipseCanvas = new p5(perlinNoiseEllipse);
const randomEllipseCanvas = new p5(randomEllipse);
const perlin1DGraphCanvas = new p5(perlinGraph1D);
const perlin2DGraphCanvas = new p5(perlinGraph2D);

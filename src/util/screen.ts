import p5 from "p5";

export const mapToScreenWidth = (
  p: p5,
  num: number,
  min: number = 0,
  max: number = 1
): number => p.map(num, min, max, 0, p.width);

export const mapToScreenHeight = (
  p: p5,
  num: number,
  min: number = 0,
  max: number = 1
): number => p.map(num, min, max, 0, p.height);

import p5 from "p5";

const setTextStyle = (p: p5) => {
  p.textSize(18);
  p.noStroke();
  p.fill(255, 255, 255);
};

export const title = (p: p5, text: string) => {
  setTextStyle(p);
  p.textAlign(p.CENTER, p.TOP);
  p.text(text, 0, 10, p.width);
};

export const footer = (p: p5, text: string) => {
  setTextStyle(p);
  p.textAlign(p.CENTER, p.BOTTOM);
  p.text(text, 0, p.height - 10, p.width);
};
